package de.quandoo.recruitment.registry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

@FixMethodOrder(MethodSorters.JVM)
public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Test
    public void testCase() {
    	//Register some customers
    	cuisinesRegistry.register(new Customer("Ali"), new Cuisine("german"));
    	cuisinesRegistry.register(new Customer("Mark"), new Cuisine("german"));
    	cuisinesRegistry.register(new Customer("Solomon"), new Cuisine("german"));
    	cuisinesRegistry.register(new Customer("Mathew"), new Cuisine("italian"));
    	cuisinesRegistry.register(new Customer("Luca"), new Cuisine("italian"));
    	cuisinesRegistry.register(new Customer("James"), new Cuisine("french"));
    	cuisinesRegistry.register(new Customer("David"), new Cuisine("french"));
    	cuisinesRegistry.register(new Customer("Solomon"), new Cuisine("french"));
    	cuisinesRegistry.register(new Customer("Mark"), new Cuisine("french"));
    	
    	//Check if Mark is registered in 2 cuisines
    	assertEquals(2,cuisinesRegistry.customerCuisines(new Customer("Mark")).size());
    	
    	//Check if french and german are the top 2 cuisines
    	List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(2);
    	assertEquals("french" , topCuisines.get(0).getName());
    	assertEquals("german" , topCuisines.get(1).getName());

    	//Check if the italian cuisine has Luca as a customer
    	assertTrue(cuisinesRegistry.cuisineCustomers(new Cuisine("italian")).contains(new Customer("Luca")));
    }
    
    @Test(expected=RuntimeException.class)
    public void testRegisterWithInvalidCuisine() {
    	cuisinesRegistry.register(new Customer("James"), new Cuisine("lebanese"));
    }
    
    @Test(expected=RuntimeException.class)
    public void testRegisterWithBadParameters() {
    	cuisinesRegistry.register(new Customer("King"), null);
    }
    
    @Test(expected=RuntimeException.class)
    public void testCustomerCuisinesWithInvalidCustomer() {
    	assertEquals(2,cuisinesRegistry.customerCuisines(new Customer("Abraham")).size());
    }
}