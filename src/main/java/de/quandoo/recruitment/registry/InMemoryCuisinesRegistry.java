package de.quandoo.recruitment.registry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.atomic.DoubleAdder;
import java.util.stream.Collectors;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.CuisineCustomerCount;
import de.quandoo.recruitment.registry.model.Customer;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

	/**
	 * Map that contains all customers and their cuisiness
	 */
    private static Map<Customer, Set<Cuisine>> customerCuisinesMap = new HashMap<>();
    
    /**
     * Set that contains all the cuisines, does not store duplicates
     */
    private static Set<Cuisine> cuisinesList = new HashSet<>();
    
    /**
     * Static block to initialize the cuisiness in the memory
     * Better to be retrieved from another data source to avoid changing the code
     */
    {
    	cuisinesList.add(new Cuisine("german"));
    	cuisinesList.add(new Cuisine("french"));
    	cuisinesList.add(new Cuisine("italian"));
    }

    @Override
    public void register(final Customer userId, final Cuisine cuisine) {
        //First check if the cuisine is available
    	cuisinesList.stream().filter(c-> c.equals(cuisine)).findFirst().orElseThrow(()->new RuntimeException("Cuisine was not found")); //Better to throw a checked exception
    	
    	if(userId == null)
    		throw new RuntimeException("Customer is not defined");
    	
    	//If yes, then register it, retrieve existing set of cuisines if available
    	Set<Cuisine> customerCuisinesList = customerCuisinesMap.get(userId);
    	if(customerCuisinesList == null) {
    		//Create new Set
    		customerCuisinesList = new HashSet<>(Arrays.asList(cuisine));
    	}
    	
    	//Add cuisine to the Set
    	customerCuisinesList.add(cuisine);
    	
    	//Store the Set into the map
		customerCuisinesMap.put(userId, customerCuisinesList);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        //First check if the cuisine is available
    	cuisinesList.stream().filter(c-> c.equals(cuisine)).findFirst().orElseThrow(()->new RuntimeException("Cuisine was not found")); //Better to throw a checked exception
    	
    	//Filter the map in order to find the customers of the cuisine
    	return  customerCuisinesMap.entrySet().stream().filter(v ->{ 
		    		//If list of cuisines contain this cuisine, then accept
			    	if(v.getValue().contains(cuisine))
			    			return true; 
			    		return false;
			    	}
    			)
    			//Map filtered results and retrieve just the customers
    			.map(k -> k.getKey())
    			//Collect the results into a list
    			.collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
    	Set<Cuisine> cuisines = customerCuisinesMap.get(customer);
    	if(cuisines == null)
    		throw new RuntimeException("Customer was not found"); //Better to throw a checked exception
    	
    	return new ArrayList<Cuisine>(cuisines);
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
    	//Prepare a set of cuisines and their count
    	Map<Cuisine, DoubleAdder> cuisinesCountsMap = new HashMap<>();
    	//Populate the map with cuisines
    	cuisinesList.forEach(cuisine-> cuisinesCountsMap.put(cuisine, new DoubleAdder()));
    	
    	//Iterate over the values
    	customerCuisinesMap.values().parallelStream().forEach(set->{
    		//For each cuisine, increment the value
    		set.forEach(cuisine-> {
    			cuisinesCountsMap.get(cuisine).add(1d);
    		});
    	});
    	

    	//Prepare a sorted set for the cuisines
    	SortedSet<CuisineCustomerCount> cuisinesCountsSortedSet = new TreeSet<>(cuisinesCountsMap.entrySet().stream().map(e-> new CuisineCustomerCount(e.getKey(), e.getValue())).collect(Collectors.toSet()));
    	
    	//Return first n results
    	return cuisinesCountsSortedSet.stream().limit(n).map(c-> c.getCuisine()).collect(Collectors.toList());
    }
}
