package de.quandoo.recruitment.registry.model;

import java.util.concurrent.atomic.DoubleAdder;

/**
 * This class is used to order the cuisines by the number of their customers 
 * @author Ali Bassam
 *
 */
public class CuisineCustomerCount implements Comparable<CuisineCustomerCount> {
	
	private final Cuisine cuisine;
    private final DoubleAdder count;
	
	public CuisineCustomerCount(Cuisine cuisine, DoubleAdder count) {
		this.cuisine = cuisine;
		this.count = count;
	}

	public Cuisine getCuisine() {
		return cuisine;
	}

	public DoubleAdder getCount() {
		return count;
	}
    
	@Override
	public int compareTo(CuisineCustomerCount o) {
		if(this.getCount().doubleValue() < o.getCount().doubleValue())
			return +1;
		if(this.getCount().doubleValue() > o.getCount().doubleValue())
			return -1;
		return 0;
	}
}
